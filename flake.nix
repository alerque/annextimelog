{
  description = "annextimelog - time tracker based on git annex";
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs = { self, nixpkgs, flake-utils, poetry2nix }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        # see https://github.com/nix-community/poetry2nix/tree/master#api for more functions and examples.
        pkgs = nixpkgs.legacyPackages.${system};
        inherit (poetry2nix.lib.mkPoetry2Nix { inherit pkgs; })
          mkPoetryApplication;
      in {
        packages = {
          annextimelog = mkPoetryApplication {
            projectDir = self;
            # TODO: preferring wheels is a non-optimal hack to get it to build. I could not for the love of it figure out how to specify these overrides and teaching that e.g. pygments needs hatchling during build. Documentation is *really* bad on this and copy-pasting random solutions from the internet does not work of course...
            preferWheels = true;
            # external dependencies that should be available to the program
            propagatedBuildInputs = with pkgs;
              [
                git
                git-annex
              ];
          };
          default = self.packages.${system}.annextimelog;
        };
        devShells.default = pkgs.mkShell {
          inputsFrom = [ self.packages.${system}.annextimelog ];
          packages = with pkgs; [ poetry ];
        };
      });
}
